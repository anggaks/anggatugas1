/// <reference types="Cypress" />

import { contains } from "cypress/types/jquery";

// import 'cypress-xpath';

export class menuipm{

    klikipm(){
        cy.get(':nth-child(2) > .oxd-main-menu-item').click();
    }

    klikadd(){
        cy.get('.orangehrm-header-container > .oxd-button').click();
    }
    firstname(){
        cy.get('.--name-grouped-field > :nth-child(1) > :nth-child(2) > .oxd-input').type('Jemboy');
    }
    middlename(){
        cy.get(':nth-child(2) > :nth-child(2) > .oxd-input').type('ganteng');
    }
    lastname(){
        cy.get(':nth-child(3) > :nth-child(2) > .oxd-input').type('bgt');
    }
    employedid(){
        cy.get('.oxd-grid-item > .oxd-input-group > :nth-child(2) > .oxd-input').type('14');
    }

    buttonsave(){
        cy.get('.oxd-button--secondary').click();
        cy.wait(3000);
    }

    oterid(){
        cy.get(':nth-child(3) > :nth-child(1) > :nth-child(2) > .oxd-input-group > :nth-child(2) > .oxd-input').type('ID135')
    }
    lisensinumber(){
        cy.get(':nth-child(3) > :nth-child(2) > :nth-child(1) > .oxd-input-group > :nth-child(2) > .oxd-input').type('QW1221')
    }

    tanggal(){
        cy.get(':nth-child(2) > .oxd-input-group > :nth-child(2) > .oxd-date-wrapper > .oxd-date-input > .oxd-input').type('2024-12-06')
    }
    kliknational(){
        cy.get(':nth-child(5) > :nth-child(1) > :nth-child(1) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text').click();
    }
    isinational(){
        cy.get('.oxd-select-dropdown > :nth-child(2) > span').contains('Afghan').click();
    }
    klikmartialstatus(){
        cy.get(':nth-child(2) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text').click();
    }
    isimartialstatus(){
        cy.get('.oxd-select-dropdown > :nth-child(2)').contains('Single').click();
    
    }
    dateofbirth(){
        cy.get(':nth-child(1) > .oxd-input-group > :nth-child(2) > .oxd-date-wrapper > .oxd-date-input > .oxd-input').type('2024-11-06')
    }
    radiobutton(){
        cy.get(':nth-child(1) > :nth-child(2) > .oxd-radio-wrapper > label > .oxd-radio-input').click();
    }
    // melitaryservice(){
    //     cy.get(':nth-child(7) > .oxd-grid-3 > :nth-child(1) > .oxd-input-group > :nth-child(2) > .oxd-input').type('full service');
    // }
    // smokerchekbox(){
    //     cy.get(':nth-child(2) > .oxd-checkbox-wrapper > label > .oxd-checkbox-input > .oxd-icon').click();
    // }
    buttonsimpanfinal(){
        cy.get(':nth-child(1) > .oxd-form > .oxd-form-actions > .oxd-button').click();
    }


menuipm1(){
    this.klikipm();
    this.klikadd();
    this.firstname();
    this.middlename();
    this.lastname();
    this.employedid();
    this.buttonsave();
    this.oterid();
    this.lisensinumber();
    this.tanggal();
    this.kliknational();
    this.isinational();
    this.klikmartialstatus();
    this.isimartialstatus();
    this.dateofbirth();
    this.radiobutton();
    // this.melitaryservice();
    // this.smokerchekbox();
    this.buttonsimpanfinal();

}

}