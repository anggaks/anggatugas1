import fs from 'fs';
import PDFDocument from 'pdfkit';

interface TestResult {
  title: string;
  state: string;
  duration: number;
}

// Membaca hasil pengujian dari file JSON
const results: TestResult[] = JSON.parse(fs.readFileSync('results.json', 'utf-8'));

// Membuat dokumen PDF
const doc = new PDFDocument();
doc.pipe(fs.createWriteStream('report.pdf'));

// Menambahkan judul dan hasil pengujian ke dalam PDF
doc.fontSize(25).text('Laporan Hasil Pengujian', { align: 'center' });
doc.moveDown();

results.forEach(test => {
  doc.fontSize(12).text(`Test: ${test.title}`);
  doc.fontSize(10).text(`Status: ${test.state}`);
  doc.fontSize(10).text(`Duration: ${test.duration} ms`);
  doc.moveDown();
});

// Selesai menulis dan menutup dokumen
doc.end();
