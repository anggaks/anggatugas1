/// <reference types="Cypress" />

export class menuadmin{

    klikadmin(){
        cy.get('#app > div.oxd-layout > div.oxd-layout-navigation > aside > nav > div.oxd-sidepanel-body > ul > li:nth-child(1) > a').click ();
    }
    inputusernamecari(){
        cy.get(':nth-child(2) > .oxd-input').type('pepatah');
    }
    klikdropdown(){
        cy.get(':nth-child(2) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text > .oxd-select-text--after > .oxd-icon').click();
    }
    isidropdownrole(){
        cy.get('.oxd-select-dropdown > :nth-child(2)').contains('Admin').click();
    }
    inputemployname(){
        cy.get('.oxd-autocomplete-text-input > input').type('Peter');
    }
    isiemplyname(){
        cy.get('.oxd-autocomplete-dropdown > :nth-child(1) > span').contains('Peter Mac Anderson').click();

    }
    klikdropdownstatus(){
        cy.get(':nth-child(4) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text > .oxd-select-text--after > .oxd-icon').click();
    }
    isidropdownstatus(){
        cy.get('.oxd-select-dropdown > :nth-child(2)').contains('Enabled').click();
    }
    klikbuttonsearch(){
        cy.get('.oxd-form-actions > .oxd-button--secondary').click();
    }
    national(){
        cy.get(':nth-child(5) > :nth-child(1) > :nth-child(1) > .oxd-input-group > :nth-child(2) > .oxd-select-wrapper > .oxd-select-text > .oxd-select-text-input').contains('Belgian');
    }


    menuadmin1(){
        this.klikadmin();
        this.inputusernamecari();
        this.klikdropdown();
        this.isidropdownrole();
        this.inputemployname();
        this.isiemplyname();
        this.klikdropdownstatus();
        this.isidropdownstatus();
        this.klikbuttonsearch();
        this.national();

    }




}